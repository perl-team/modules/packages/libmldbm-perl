libmldbm-perl (2.05-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmldbm-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 01:48:51 +0100

libmldbm-perl (2.05-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:39:05 +0100

libmldbm-perl (2.05-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 01 Jan 2021 15:30:02 +0100

libmldbm-perl (2.05-2) unstable; urgency=medium

  * Team upload.

  [ Daniel Lintott ]
  * d/control, d/copyright, d/watch - Correct metacpan
                           URL and drop trailing slash

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Raphaël Hertzog ]
  * Drop myself from Uploaders.

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Mon, 08 Jun 2015 21:07:04 +0300

libmldbm-perl (2.05-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 2.05
  * Bump years of upstream copyright
  * Update stand-alone License paragraphs to commonly used versions (not
    mentioning "GNU/Linux", directly linking GPL-1)
  * Bump dh compatibility to level 8 (no changes necessary)
  * Declare compliance with Debian Policy 3.9.4
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Thu, 03 Oct 2013 20:42:04 +0200

libmldbm-perl (2.04-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: add upstream email address.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Mar 2010 19:43:57 +0200

libmldbm-perl (2.03-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: refresh formatting and list of upstream copyright
    holders.
  * Add testmldbm.pag to debian/clean, remove override in debian/rules, lower
    debhelper depenceny in debian/control. Set debian/compat to 7, with 6
    debian/clean is ignored. Also remove testmldbm and testmldbm.dir,
    otherwise the package fails to build twice.
  * Add /me to Uploaders.
  * debian/control: make short description a noun phrase.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Mar 2010 17:26:06 +0100

libmldbm-perl (2.02-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.

  [ Raphaël Hertzog ]
  * New upstream release (orig.tar.gz created from upstream .zip and converted
    to Unix EOL).
  * Switch to "3.0 (quilt)" source format.
  * Update Standards-Version to 3.8.4 (no further change required).

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 23 Feb 2010 14:02:02 +0100

libmldbm-perl (2.01-3) unstable; urgency=low

  [ gregor herrmann ]
  * debian/watch: use dist-based URL.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Raphaël Hertzog ]
  * Switch to debhelper 7 tiny rules file.
  * Update my name in Uploaders to have the required accent.
  * Update Standards-Version to 3.8.3 (no further change required).

 -- Raphaël Hertzog <hertzog@debian.org>  Sun, 20 Sep 2009 15:24:37 +0200

libmldbm-perl (2.01-2) unstable; urgency=low

  [ Raphael Hertzog ]
  * Put the team as Maintainer and move myself to Uploaders.
  * Drop minimal version from dependencies where it's useless since
    oldstable already has the required version
  * Updated Standards-Version to 3.7.3
    - Always build with -g and don't check for "debug" in DEB_BUILD_OPTIONS.
  * Don't ignore errors on make distclean.
  * Switch to debhelper's 6th compatilitiy level. Adjust Build-Depends
    accordingly. Use debian/compat insteald of DH_COMPAT.
  * Remove empty /usr/lib/perl5 if present.
  * Move to Section: perl to match with overrides.
  * Updated copyright file to refer to Artistic and GPL licenses.

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.

 -- Raphael Hertzog <hertzog@debian.org>  Sun, 03 Feb 2008 21:06:40 +0100

libmldbm-perl (2.01-1) unstable; urgency=low

  * New upstream version.
  * Launch make test at compile time. Updated buil-depends accordingly.
  * Added recommends to perl-modules and libfreezethaw-perl.

 -- Raphael Hertzog <hertzog@debian.org>  Sat,  2 Nov 2002 10:39:53 +0100

libmldbm-perl (2.00-10) unstable; urgency=low

  * Added a debian/watch file.
  * Complies with policy 3.5.7.
  * Removed Recommends as libstorable-perl is integrated in perl 5.8.
    Closes: #159203
  * Removed useless README.Debian.

 -- Raphael Hertzog <hertzog@debian.org>  Mon,  2 Sep 2002 00:23:50 +0200

libmldbm-perl (2.00-9) unstable; urgency=medium

  * Updated to follow the latest perl policy.
  * Complies with policy 3.5.2.

 -- Raphael Hertzog <hertzog@debian.org>  Wed, 21 Feb 2001 22:55:43 +0100

libmldbm-perl (2.00-8) unstable; urgency=low

  * Complies to latest policy. Now conflicts with a too old perl.
    Do not depend anymore on a specific perl version.

 -- Raphael Hertzog <hertzog@debian.org>  Wed, 27 Dec 2000 09:41:22 +0100

libmldbm-perl (2.00-7) unstable; urgency=low

  * Removed FreezeThaw.pm. Closes: #63002
  * Updated copyright file (lintian warning).

 -- Raphael Hertzog <hertzog@debian.org>  Thu, 13 Jul 2000 15:23:00 +0200

libmldbm-perl (2.00-6) unstable; urgency=low

  * Policy 3.0.1 compliance. Built with debhelper 2.0.40.

 -- Raphael Hertzog <hertzog@debian.org>  Thu,  9 Sep 1999 13:28:45 +0200

libmldbm-perl (2.00-5) unstable; urgency=low

  * Updated the build process to respect the new perl-policy.

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Tue, 22 Jun 1999 00:51:57 +0200

libmldbm-perl (2.00-4) unstable; urgency=low

  * Re-corrected the dependencies for perl-5.005, my first guess was
    wrong.

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Mon, 14 Jun 1999 13:39:45 +0200

libmldbm-perl (2.00-3) unstable; urgency=low

  * Corrected the dependencies so that switching to perl5.005 does not
    break.

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Tue,  9 Feb 1999 21:53:59 +0100

libmldbm-perl (2.00-2) unstable; urgency=low

  * Corrected two typos : s/multidimensionnal/multidimensional/

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Tue, 22 Dec 1998 22:50:13 +0100

libmldbm-perl (2.00-1) unstable; urgency=low

  * Initial Release.

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Thu, 22 Oct 1998 15:18:56 +0200
